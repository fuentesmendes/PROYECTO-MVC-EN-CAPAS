﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xynthesis.Modelo;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using PagedList.Mvc;
using PagedList;
using System.IO;
using Xynthesis.AccesoDatos;
using Xynthesis.Utilidades;

namespace Xynthesis.Web.Controllers
{
    public class ReporteConsumosPersonalesController : Xynthesis.Web.Models.FormatoReporte
    {
        Xynthesis.Utilidades.Mensaje msg = new Mensaje();
        Utilidades.LogXynthesis log = new LogXynthesis();
        Utilidades.Constantes cons = new Constantes();
        xynthesisEntities xyt = new xynthesisEntities();
        ADReporteConsumosPersonales Consper= new ADReporteConsumosPersonales();
        public int contador;
        // GET: ReporteConsumosPersonales
        public ActionResult ConsumosPersonales(string paraPaginacion, string filtro, string extension, string FechaInicial, string FechaFinal, int? page)
        {
            if (Session["Ide_Subscriber"] == null)
            {
                return RedirectToAction("Login", "Acceso");
            }
            ViewData["usuario"] = (from t in xyt.xy_subscriber
                                   where t.Ide_Subscriber != -1
                                   orderby t.Nom_Subscriber ascending
                                   select t).ToList();
            //Inicio de lineas agregadas
            if (Session["FechaInicial"] != null)
            {
                contador++;
            }

            int valor = contador;

            if (FechaInicial == null & Session["FechaInicial"] != null & valor == 1 & page == null)
            {
                Session["FechaInicial"] = null;
                Session["FechaFinal"] = null;
                Session["usuarios"] = null;
            }

            if (FechaInicial == null & Session["FechaInicial"] != null & valor == 0 & page == null)
            {
                Session["FechaInicial"] = null;
                Session["FechaFinal"] = null;
                Session["usuarios"] = null;
            }
            if (Session["Extension"] != null)
            {
                extension = Session["Extension"].ToString();
            }
                //if (FechaInicial == null & Session["FechaInicial"] != null & valor >= 1)
                //{
                //}
                //Fin de lineas agregadas

                List<xyp_SelConsumeByExtensionAndUser_Result> lista;
            try
            {
                if (Session["FechaInicial"] != null && Session["FechaFinal"] != null)
                    lista = Consper.ObtenerConsumosPersonales(Session["FechaInicial"].ToString(), Session["FechaFinal"].ToString(), extension, Session["usuarios"].ToString()).ToList();
                else
                    lista = Consper.ObtenerConsumosPersonales( null, null,null, null).ToList();

                int pageSize = cons.MaxRegGrilla == null ? 8 : Convert.ToInt32(cons.MaxRegGrilla);
                int pageNumber = (page ?? 1);

                ViewBag.fechaini = Session["FechaInicial"];
                ViewBag.fechafin = Session["FechaFinal"];
                ViewBag.extension = Session["Extension"];

                return View(lista.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                log.EscribaLog("REPORTE", "Action:ConsumosPersonales " + ex.Message, Session["Nom_DomainUser"].ToString());
                throw ex;
            }
        }

        public ActionResult ConsumosPersonales_(string extension,string FechaInicial, string FechaFinal, string[] usuarioId, int? page)
        {
            string usuario = "";
            string user;
            if (usuarioId == null)
            {
                user = "";
            }
            else
            {
                for (var i = 0; i < usuarioId.Length; i++)
                {
                    usuario += usuarioId[i].ToString() + "|";
                }
                user = usuario;
            }

            Session["usuarios"] = user;
            //user = usuario;
            //Char separador = '|';
            //String[] ides = usuario.Split(separador);

            //String[] ides = usuarioId;

            ViewData["usuario"] = (from t in xyt.xy_subscriber
                                   where t.Ide_Subscriber != -1
                                   orderby t.Nom_Subscriber ascending
                                   select t).ToList();

            try
            {
                List<xyp_SelConsumeByExtensionAndUser_Result> lista = Consper.ObtenerConsumosPersonales(FechaInicial, FechaFinal, extension, user).ToList();
                int pageSize = cons.MaxRegGrilla == null ? 8 : Convert.ToInt32(cons.MaxRegGrilla);
                int pageIndex = 1;
                int pageNumber = (page ?? 1);
                pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

                Session["FechaInicial"] = FechaInicial;
                Session["FechaFinal"] = FechaFinal;
                Session["Extension"] = extension;

                ViewBag.fechaini = Session["FechaInicial"];
                ViewBag.fechafin = Session["FechaFinal"];
                ViewBag.extension = Session["Extension"];

                return View("ConsumosPersonales", lista.ToPagedList(pageIndex, pageSize));

            }
            catch (Exception ex)
            {
                log.EscribaLog("REPORTE", "Action:ConsumosPersonales " + ex.Message, Session["Nom_DomainUser"].ToString());
                throw ex;
            }
        }

        public ActionResult Reportes(string opcion)
        {
            if (Session["FechaInicial"] == null || Session["FechaFinal"] == null)
                return View("ConsumosPersonales", new List<xyp_SelConsumeByExtensionAndUser_Result>().ToPagedList(1, 1));
            else
                return ReporteFormato(opcion, "ConsumosPersonales", "ObtenerConsumosPersonales",
                    Session["FechaInicial"].ToString(), Session["FechaFinal"].ToString(), Session["Extension"].ToString(), Session["usuarios"].ToString());
        }


    }
}