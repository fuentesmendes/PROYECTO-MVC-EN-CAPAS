﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xynthesis.Modelo;
using Xynthesis.Utilidades;

namespace Xynthesis.Web.Controllers
{
    public class ReportesMomentoController : Controller
    {
        Utilidades.LogXynthesis log = new LogXynthesis();
        Xynthesis.Utilidades.Mensaje msg = new Mensaje();
        xynthesisEntities xyt = new xynthesisEntities();
        public int num;
        public string nomrpt, metodopt;
        // GET: ReportesMomento
        public ActionResult ReportesMomentaneos()
        {
            if (Session["Ide_Subscriber"] == null)
            {
                return RedirectToAction("Login", "Acceso");
            }
            var lista = xyt.xy_reportes.ToList();
            return View(lista);
        }

        
        public ActionResult ReportesMomentaneosprint(string idd, string formato, string FechaInicial, string FechaFinal)
        {
            devolverlista(1);
            List<xyp_NumberAmountsByInSubscriber_Result> lista;
            lista = xyt.xyp_NumberAmountsByInSubscriber(FechaInicial,FechaFinal,"","","").ToList();

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reportes"), "ReporteLlamadasEntrantes.rpt"));

            rd.SetDataSource(lista);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            try
            {
                if(formato == "pdf")
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "ReporteLlamadasEntrantes.pdf");
                }else
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/xls", "ReporteLlamadasEntrantes.xls");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        } 
        
        public FileStreamResult reporte(int id, string formato, string FechaInicial, string FechaFinal)
        {

            List<xyp_NumberAmountsByInSubscriber_Result> lista = new List<xyp_NumberAmountsByInSubscriber_Result>();
            lista = xyt.xyp_NumberAmountsByInSubscriber(FechaInicial, FechaFinal, "", "", "").ToList();

            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reportes"), "ReporteLlamadasEntrantes.rpt"));

            rd.SetDataSource(lista);

            Response.Buffer = false;
            Response.ClearContent();
            Response.ClearHeaders();

            try
            {
                if (formato == "pdf")
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/pdf", "ReporteLlamadasEntrantes.pdf");
                }
                else
                {
                    Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel);
                    stream.Seek(0, SeekOrigin.Begin);
                    return File(stream, "application/xls", "ReporteLlamadasEntrantes.xls");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        
        public string devolverlista(int id)
        {
            if(id==1)
            {
                List<xyp_SelCallAmountsBySubscriber_Result> lista;
                lista = xyt.xyp_SelCallAmountsBySubscriber("", "", "").ToList();
                return 
            }else
            {
                List<xyp_SelCallAmountsBySubscriber_Result> lista;
                lista = xyt.xyp_SelCallAmountsBySubscriber("", "", "").ToList();
            }
            return List<lista>;
        }   


    }
}