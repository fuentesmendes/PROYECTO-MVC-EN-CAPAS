﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xynthesis.Modelo;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using PagedList.Mvc;
using PagedList;
using System.IO;
using Xynthesis.Utilidades;
using Xynthesis.AccesoDatos;

namespace Xynthesis.Web.Controllers
{
    public class RepPromedioLlamadasHoraController :Xynthesis.Web.Models.FormatoReporte
    {
        xynthesisEntities xyt = new xynthesisEntities();
        ADRepPromedioLlamadasHora prollama = new ADRepPromedioLlamadasHora();
        LogXynthesis log = new LogXynthesis();
        Constantes cons = new Constantes();
        public int contador;
        // GET: RepPromedioLlamadasHora
        public ActionResult ListaPromedioLlamadaHora(string paraPaginacion, string filtro, string FechaInicial, string FechaFinal, int? page)
        {

            if (Session["Ide_Subscriber"] == null)
            {
                return RedirectToAction("Login", "Acceso");
            }

            //Inicio de lineas agregadas
            if (Session["FechaInicial"] != null)
            {
                contador++;
            }

            int valor = contador;

            if (FechaInicial == null & Session["FechaInicial"] != null & valor == 1 & page == null)
            {
                Session["FechaInicial"] = null;
                Session["FechaFinal"] = null;
            }

            if (FechaInicial == null & Session["FechaInicial"] != null & valor == 0 & page == null)
            {
                Session["FechaInicial"] = null;
                Session["FechaFinal"] = null;
            }

            //if (FechaInicial == null & Session["FechaInicial"] != null & valor >= 1)
            //{
            //}
            //Fin de lineas agregadas

            try
            {
                List<xyp_RepPromedioLlamadasHora_Result> lista;

                if (Session["FechaInicial"] != null && Session["FechaFinal"] != null)
                    lista = prollama.ObtenerPromedioLlamadasHora(Session["FechaInicial"].ToString(), Session["FechaFinal"].ToString()).ToList();
                else
                    lista = prollama.ObtenerPromedioLlamadasHora(null, null).ToList();

                int pageSize = 10;
                int pageNumber = (page ?? 1);

                ViewBag.fechaini = Convert.ToDateTime(Session["FechaInicial"]).ToString("dd-MM-yyyy");
                ViewBag.fechafin = Convert.ToDateTime(Session["FechaFinal"]).ToString("dd-MM-yyyy");

                if (ViewBag.fechaini != "01-01-0001" && ViewBag.fechafin != "01-01-0001")
                {
                    ViewBag.fechaini = Convert.ToDateTime(Session["FechaInicial"]).ToString("dd-MM-yyyy") + " A";
                    ViewBag.fechafin = Convert.ToDateTime(Session["FechaFinal"]).ToString("dd-MM-yyyy");
                }
                else
                {
                    ViewBag.fechaini = null;
                    ViewBag.fechafin = null;
                }

                return View(lista.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception ex)
            {
                log.EscribaLog("REPORTE", "Action:ListaPromedioLlamadaHora " + ex.Message, Session["Nom_DomainUser"].ToString());
                throw ex;
            }
        }

        public ActionResult ListaPromedioLlamadaHora_(string FechaInicial, string FechaFinal, int? page)
        {
            if (FechaInicial == "" || FechaFinal == "")
            {
                return RedirectToAction("ListaPromedioLlamadaHora", "RepPromedioLlamadasHora");
            }
            else
            {
                List<xyp_RepPromedioLlamadasHora_Result> lista = prollama.ObtenerPromedioLlamadasHora(FechaInicial, FechaFinal).ToList();
                int pageSize = 10;
                int pageIndex = 1;
                int pageNumber = (page ?? 1);
                pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

                Session["FechaInicial"] = FechaInicial;
                Session["FechaFinal"] = FechaFinal;

                try
                {
                    ViewBag.fechaini = Convert.ToDateTime(Session["FechaInicial"]).ToString("dd-MM-yyyy") + " A";
                    ViewBag.fechafin = Convert.ToDateTime(Session["FechaFinal"]).ToString("dd-MM-yyyy");
                }
                catch (Exception ex)
                {

                }
                return View("ListaPromedioLlamadaHora", lista.ToPagedList(pageIndex, pageSize));
            }
        }


        public ActionResult Reportes(string opcion)
        {
            if (Session["FechaInicial"] == null || Session["FechaFinal"] == null)
                return View("ListaPromedioLlamadaHora", new List<xyp_RepPromedioLlamadasHora_Result>().ToPagedList(1, 1));
            else
                return ReporteFormato(opcion, "RepPromedioLlamadasHora", "ObtenerPromedioLlamadasHora",
                    Session["FechaInicial"].ToString(), Session["FechaFinal"].ToString());
            //Reportes_(opcion, Session["FechaInicial"].ToString(), Session["FechaFinal"].ToString(), "RepPromedioLlamadasHora");
        }

        public JsonResult ConPromediolla()
        {
            List<xyp_RepPromedioLlamadasHora_Result> lista;
            try
            {
                if (Session["FechaInicial"] != null && Session["FechaFinal"] != null)
                    lista = prollama.ObtenerPromedioLlamadasHora(Session["FechaInicial"].ToString(), Session["FechaFinal"].ToString()).ToList();
                else
                    lista = prollama.ObtenerPromedioLlamadasHora(null, null).ToList();

                var res_ = from s in lista  select s;

                return Json(res_, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                log.EscribaLog("REPORTE", "Action:ConPromediolla " + ex.Message, Session["Nom_DomainUser"].ToString());
                throw ex;
            }

        }



    }
}