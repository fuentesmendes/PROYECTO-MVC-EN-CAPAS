﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xynthesis.AccesoDatos;
using Xynthesis.Modelo;

namespace Xynthesis.ServicioReportes
{
    public partial class Service1 : ServiceBase
    {
        public DateTime fechaEjecucion = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
        public string diaEjecucion = ConfigurationManager.AppSettings["diaEjecucion"] as string;
        Timer Schedular;
        int count;
        public Service1()
        {
            InitializeComponent();
     

        }
        private void SchedularCallback(object e)
        {
           
            this.WorkProcess();
        }
        public void WorkProcess()
        {
            Schedular = new Timer(new TimerCallback(SchedularCallback));
            Int64 dueTime = 0;
            TimeSpan timeSpan;
            try
            {

                if (DateTime.Now.Day == Convert.ToInt32(diaEjecucion))
                {
                    //this.LogService("dia");
                    ADBase contexto = new ADBase();

                    List<xyp_SelReports_Result> lstReportes = new List<xyp_SelReports_Result>();

                    lstReportes = contexto.ObtenerProgramacionReportes();
                    var Horas = lstReportes.Select(P => Convert.ToDateTime(P.HoraEjecucion).TimeOfDay).ToList().Distinct();
                    var HorMin = Horas.Select(P => P).Min();
                    var HorMax = Horas.Select(P => P).Max();
                    DateTime t2 = new DateTime(fechaEjecucion.Year, fechaEjecucion.Month, fechaEjecucion.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0, 0);
                    this.LogService("Ejecucion de reportes");
                    foreach (var rpt in Horas.OrderBy(P => true).ToList())
                    {
                        if (t2.TimeOfDay == rpt)
                        {
                            var reportesHora = lstReportes.Where(P => Convert.ToDateTime(P.HoraEjecucion).TimeOfDay == rpt).ToList();
                            var reportesConf = reportesHora.Select(P => P.ConfiguracionId).Distinct();
                            DateTime fechaFinal = DateTime.Today.AddDays(-1);
                            string FechaIni = fechaFinal.AddMonths(-1).Date.ToString("yyyy-MM-dd");
                            string FechaFin = fechaFinal.Date.ToString("yyyy-MM-dd");
                            foreach (var conf in reportesConf)
                            {
                                try
                                {
                                    var reportesProgramados = reportesHora.Where(P => P.ConfiguracionId == conf).ToList();
                                    Xynthesis.Reportes.ExportacionReportes.GenerarReporteProgramado(reportesProgramados, FechaIni, FechaFin);
                                }
                                catch (Exception ex)
                                {
                                    this.LogService("Error en ejecucion de reporte ConfiguracionID:" + conf + " Error: " + ex.Message + " StackTrace:" + ex.StackTrace);
                                }
                            }
                            if (rpt == HorMax)
                            {

                                DateTime fecInicial = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd") + " " + HorMin.Hours.ToString() + ":" + HorMin.Minutes.ToString() + ":00");
                                DateTime fecFinal = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd") + " " + HorMin.Hours.ToString() + ":" + HorMin.Minutes.ToString() + ":00");// " 00:01:00")
                                //Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd") + " 00:01:00");//DateTime.Now.Date;
                                fecFinal = fecFinal.AddMonths(1);
                                timeSpan = fecFinal.Subtract(fecInicial);
                                dueTime = Convert.ToInt64(timeSpan.TotalMilliseconds);
                                this.LogService("Proxima Fecha de ejecucion" + fecFinal.ToString());
                                Schedular.Change(dueTime, Timeout.Infinite);
                                return;
                            }

                        }
                        else if (rpt > t2.TimeOfDay)
                        {

                            DateTime fec = new DateTime(t2.Year, t2.Month, t2.Day, rpt.Hours, rpt.Minutes, 0, 0);
                            timeSpan = fec.Subtract(DateTime.Now);
                            dueTime = Convert.ToInt64(timeSpan.TotalMilliseconds);
                            Schedular.Change(dueTime, Timeout.Infinite);

                            return;
                        }


                    }

                }
                else
                {
                    DateTime fec = new DateTime(DateTime.Now.Year, DateTime.Now.Month+1, 1, 0, 0, 0, 0);
                    timeSpan = fec.Subtract(DateTime.Now.Date);
                    dueTime = Convert.ToInt64(timeSpan.TotalMilliseconds);
                    this.LogService("Proxima Fecha de ejecucion" + fec.ToString());
                    Schedular.Change(dueTime, Timeout.Infinite);
                    //Schedular.Change(0, Timeout.Infinite);
                }
            }
            catch (Exception ex)
            {
                LogService("Error No Controlado: {0} " + ex.Message + ex.StackTrace);

                using (System.ServiceProcess.ServiceController serviceController = new System.ServiceProcess.ServiceController("SimpleService"))
                {
                    serviceController.Stop();
                }
            }
        }
        protected override void OnStart(string[] args)
        {
            LogService("El Servicio se ha Iniciado");
            this.WorkProcess();

        }
        protected override void OnStop()
        {
            LogService("El Servicio se ha parado");

            this.Schedular.Dispose();
        }
        private void LogService(string content)
        {
            string RutaLog = ConfigurationManager.AppSettings["RutaLogServiceRpt"] as string;
            FileStream fs = new FileStream(RutaLog, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            sw.BaseStream.Seek(0, SeekOrigin.End);
            sw.WriteLine(content);
            sw.Flush();
            sw.Close();
        }
    }
}
