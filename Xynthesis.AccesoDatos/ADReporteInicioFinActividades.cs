﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xynthesis.Modelo;
using System.Data.Entity;
using Xynthesis.Utilidades;

namespace Xynthesis.AccesoDatos
{
   public class ADReporteInicioFinActividades
    {
        xynthesisEntities xyt = new xynthesisEntities();
        Mensaje msg = new Mensaje();
        LogXynthesis log = new LogXynthesis();

        public List<xyp_SelActivityFirstAndLast_Result> ObtenerListaInicioFin(string FechaInicial, string FechaFinal, string usuario)
        {
            //try
            //{
            //    var qResultado = xyt.xyp_SelActivityFirstAndLast(FechaInicial, FechaFinal, usuario).ToList();
            //    List<xyp_SelActivityFirstAndLast_Result> lsLista = new List<xyp_SelActivityFirstAndLast_Result>();
            //    foreach (var item in qResultado)
            //    {
            //        xyp_SelActivityFirstAndLast_Result elemento = new xyp_SelActivityFirstAndLast_Result();
            //        DateTime dtI = DateTime.Parse(item.FechaInicio);
            //        DateTime dtF = DateTime.Parse(item.FechaFin);
            //        TimeSpan tsDiferencia = dtF.Subtract(dtI);
            //        elemento.Duracion = string.Format("{0:hh\\:mm\\:ss}", tsDiferencia);
            //        elemento.FechaFin = item.FechaFin;
            //        elemento.FechaInicio = item.FechaInicio;
            //        elemento.Fec_Date = item.Fec_Date;
            //        elemento.Nom_Subscriber = item.Nom_Subscriber;
            //        lsLista.Add(elemento);

            //    }
            //    return lsLista;
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}



            try
            {
                return xyt.xyp_SelActivityFirstAndLast(FechaInicial, FechaFinal, usuario).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
